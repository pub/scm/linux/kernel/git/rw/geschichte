// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;

#[derive(Serialize, Deserialize)]
pub struct PlayListConfig {
	pub name: String,
	pub source: String,
	pub program: String,
	pub keycode: u32,
	#[serde(default)]
	pub extra: HashMap<String, String>,
}

#[derive(Serialize, Deserialize)]
pub struct PlayerConfig {
	pub base: HashMap<String, String>,
	pub keymap: HashMap<String, u32>,
	pub backendconfig: HashMap<String, String>,
	pub playlist: Vec<PlayListConfig>,
}

pub fn init_cfg() -> Option<PlayerConfig> {
	let cfg_text = match fs::read_to_string("geschichte.toml") {
		Err(e) => {
			eprintln!("Unable to read geschichte.toml: {}", e);
			return None;
		}
		Ok(v) => v,
	};

	match toml::from_str(&cfg_text) {
		Err(e) => {
			eprintln!("Unable to parse config file: {}", e);
			None
		}
		Ok(cfg) => Some(cfg),
	}
}
