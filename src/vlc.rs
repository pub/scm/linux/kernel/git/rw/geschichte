// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use std::io;
use std::io::Read;
use std::io::Write;
use std::net::SocketAddr;
use std::net::TcpStream;
use std::net::ToSocketAddrs;
use std::os::unix::io::AsRawFd;
use std::path::PathBuf;
use std::time::Duration;

use mio::event::Event;
use mio::{Events, Interest, Poll, Token};

use crate::config::PlayerConfig;
use crate::playerbackend::PlayerBackend;

pub struct BackendVlc {
	conn: mio::net::TcpStream,
	mio_poll: Poll,
	volume: u32,
	volume_step: u32,
}

impl BackendVlc {
	pub fn new(cfg: &PlayerConfig) -> Option<BackendVlc> {
		let vlcaddr = match cfg.backendconfig.get("addr") {
			None => {
				eprintln!("addr not set in backendconfig section!");
				return None;
			}
			Some(v) => v,
		};

		match BackendVlc::init(vlcaddr) {
			Ok(vlc) => Some(vlc),
			Err(e) => {
				eprintln!("Unable to connect to VLC: {}", e);
				None
			}
		}
	}

	fn init(dst: &str) -> io::Result<BackendVlc> {
		let addrs: Vec<SocketAddr> = dst
			.to_socket_addrs()
			.expect("Unable to resolve VLC hostname(s)")
			.collect();

		let mut conn: Option<TcpStream> = None;
		for addr in &addrs {
			let ct = TcpStream::connect_timeout(&addr, Duration::new(5, 0));
			match ct {
				Err(e) => {
					eprintln!(
						"Unable to connect to VLC at {}: {}. Trying next address",
						addr, e
					);
				}
				Ok(c) => {
					c.set_nonblocking(true)?;
					conn = Some(c);
					eprintln!("Connected to VLC at {}", addr);
					break;
				}
			}
		}

		if conn.is_none() {
			return Err(io::Error::new(
				io::ErrorKind::Other,
				"No connection to VLC was possible",
			));
		}

		let mut mio_conn = mio::net::TcpStream::from_std(conn.unwrap());

		let poll = Poll::new()?;
		poll.registry()
			.register(&mut mio_conn, Token(0), Interest::READABLE)?;

		let mut vlc = BackendVlc {
			conn: mio_conn,
			mio_poll: poll,
			volume: 256,
			volume_step: 5,
		};

		vlc.wait_till_ready()?;
		vlc.do_vlc_cmds(vec![String::from("stop"), String::from("clear")])?;
		vlc.set_volume(vlc.volume)?;

		Ok(vlc)
	}

	fn proc_vlc_input(
		&mut self,
		event: &Event,
		buf: &mut Vec<u8>,
		nread: &mut usize,
	) -> io::Result<bool> {
		if event.is_error() {
			return Err(io::Error::new(
				io::ErrorKind::Other,
				"Connection to VLC gone",
			));
		} else if event.is_readable() {
			loop {
				match self.conn.read(&mut buf[*nread..]) {
					Ok(n) => {
						*nread += n;
						if buf.len() <= *nread {
							buf.resize(buf.len() * 2, 0);
						}
					}
					Ok(0) => break,
					Err(ref err) if err.kind() == io::ErrorKind::WouldBlock => break,
					Err(ref err) if err.kind() == io::ErrorKind::Interrupted => continue,
					Err(err) => return Err(err),
				}
			}
			let resp = String::from_utf8_lossy(&buf[..*nread]);
			if resp.ends_with("> ") {
				return Ok(true);
			}
		}

		Ok(false)
	}

	fn wait_till_ready(&mut self) -> io::Result<()> {
		let mut total = vec![0; 128];
		let mut nread = 0;
		let mut events = Events::with_capacity(5);

		loop {
			self.mio_poll.poll(&mut events, None)?;

			for event in events.iter() {
				match event.token() {
					Token(0) => {
						if self.proc_vlc_input(&event, &mut total, &mut nread)? == true {
							return Ok(());
						}
					}
					_ => {}
				}
			}
		}

		Ok(())
	}

	fn do_vlc_cmds(&mut self, cmds: Vec<String>) -> io::Result<()> {
		for cmd in cmds {
			self.conn.write(format!("{}\n", cmd).as_bytes())?;
			self.wait_till_ready()?;
		}

		Ok(())
	}
}

impl PlayerBackend for BackendVlc {
	fn set_volume(&mut self, vol: u32) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("volume {}", vol)])
	}

	fn volume_up(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("volup {}", self.volume_step)])
	}

	fn volume_down(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![format!("voldown {}", self.volume_step)])
	}

	fn pause(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("pause")])
	}

	fn stop(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("stop")])
	}

	fn play(&mut self) -> io::Result<()> {
		self.do_vlc_cmds(vec![String::from("play")])
	}

	fn play_single_file(&mut self, file: &PathBuf) -> io::Result<()> {
		self.play_multiple_files(vec![file])
	}

	fn play_multiple_files(&mut self, files: Vec<&PathBuf>) -> io::Result<()> {
		let mut cmds = Vec::new();

		cmds.push(String::from("stop"));
		cmds.push(String::from("clear"));

		for file in files {
			let f = file.to_str().unwrap();

			cmds.push(format!("enqueue {}", f));
		}

		cmds.push(String::from("goto 0"));
		cmds.push(String::from("play"));

		self.do_vlc_cmds(cmds)
	}
}
