// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

use std::io;
use std::path::PathBuf;

pub trait PlayerBackend {
	fn set_volume(&mut self, vol: u32) -> io::Result<()>;
	fn volume_up(&mut self) -> io::Result<()>;
	fn volume_down(&mut self) -> io::Result<()>;
	fn pause(&mut self) -> io::Result<()>;
	fn stop(&mut self) -> io::Result<()>;
	fn play(&mut self) -> io::Result<()>;
	fn play_single_file(&mut self, file: &PathBuf) -> io::Result<()>;
	fn play_multiple_files(&mut self, files: Vec<&PathBuf>) -> io::Result<()>;
}
