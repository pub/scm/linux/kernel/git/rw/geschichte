// SPDX-License-Identifier: GPL-2.0-only
/*
 *  Copyright (C) 2021, Richard Weinberger <richard@nod.at>
 */

#![allow(dead_code)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::fs::File;
use std::fs::OpenOptions;
use std::io::Read;
use std::io;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::io::AsRawFd;

use mio::unix::SourceFd;
use mio::{Events, Interest, Poll, Token};

pub trait InputEventRecv {
	fn consume_input(&mut self, code: u32, value: i32) -> io::Result<bool>;
}

fn do_fd_event(input: &mut File, ier: &mut dyn InputEventRecv) -> io::Result<bool> {
	let struct_size = std::mem::size_of::<input_event>();
	let mut buffer = vec![0; struct_size];
	let mut events = Vec::new();

	loop {
		match input.read_exact(&mut buffer) {
			Ok(()) => {
				let s: input_event = unsafe { std::ptr::read(buffer.as_ptr() as *const _) };
				events.push(s);
			}
			Err(ref err) if err.kind() == io::ErrorKind::WouldBlock => break,
			Err(ref err) if err.kind() == io::ErrorKind::Interrupted => continue,
			Err(err) => return Err(err)
		}
	}

	for ev in events {
		let ev_type: u32 = ev.type_ as u32;
		if ev_type == EV_KEY {
			match ier.consume_input(ev.code as u32, ev.value as i32) {
				Ok(true) => { },
				Ok(false) => { return Ok(false); },
				Err(e) => { return Err(e); },
			}
		}
	}

	Ok(true)
}

pub fn do_events(inputdev: &str, ier: &mut dyn InputEventRecv) -> io::Result<()> {
	let mut input = OpenOptions::new()
		.read(true)
		.custom_flags(libc::O_NONBLOCK)
		.open(&inputdev)?;


	let mut events = Events::with_capacity(1);
	let mut poll = Poll::new()?;
	poll.registry().register(&mut SourceFd(&input.as_raw_fd()), Token(0), Interest::READABLE)?;

	loop {
		poll.poll(&mut events, None)?;

		if events.is_empty() {
			continue;
		}

		for event in events.iter() {
			match event.token() {
				Token(0) => {
					match do_fd_event(&mut input, ier) {
						Ok(_) => {},
						Ok(false) => { return Ok(()); },
						Err(e) => { return Err(e); },
					}
				},
				_ => {},
			}
		}
	}
}
